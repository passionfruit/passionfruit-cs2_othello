#include "exampleplayer.h"

/// currently, we are not dealing with the time component. we need to...
/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
ExamplePlayer::ExamplePlayer(Side side) {
    /* 
     * TODO: Do any initialization you need to do here (setting up the board,
     * precalculating things, etc.) However, remember that you will only have
     * 30 seconds.
     */

    //initialize the board here


    board = new Board();
    our_side = side;

    if(our_side == BLACK)
    {
        their_side = WHITE;
    }
    else
    {
        their_side = BLACK;
    }

    //precalculate things here
    //do these things quickly

}

/*
 * Destructor for the player.
 */
ExamplePlayer::~ExamplePlayer() {
}


/*
 * Compute the next move given the opponent's last move. Each AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * If there are no valid moves for your side, doMove must return NULL.
 *
 * Important: doMove must take no longer than the timeout passed in 
 * msLeft, or your AI will lose! The move returned must also be legal.
 */
Move *ExamplePlayer::doMove(Move *opponentsMove, int msLeft) {
    /* 
     * TODO: Implement how moves your AI should play here. You should first
     * process the opponent's opponents move before calculating your own move

     */
    struct timeval start_time, current_time, result;
    double time_elapsed = 0.0;
    gettimeofday(&start_time, NULL); //start the clock

    // deal with oppents move
    board->doMove(opponentsMove, their_side);

    //do we have any legal moves we can make?
    //attempt to make a move
    for (int i = 0; i <= 7; i++)
    {
        for (int j = 0; j <= 7; j++)
        {
            Move *move = new Move(i, j);
            if (board->checkMove(move, our_side))
            {
                //add this move to our vector of all possible moves
                possibleMoves.push_back(move);
            }
        }
     }
    // now iterate over possibleMoves
    vector<Move*>::iterator i;
    int maximumScore = -100000;
    vector<Move*>::iterator bestMoveLocation;

    /// when calculating time, do this:
    gettimeofday(&current_time, NULL); // define now
    timersub(&current_time, &start_time, &result);
    // calculate time_elapsed in milliseconds
    time_elapsed = (result.tv_usec / 1000.0) + (result.tv_sec * 1000.0);

    if((msLeft - time_elapsed) < 2.0 && msLeft != 0) // if we run out of time, move randomly
    {
        Move * randomMove = possibleMoves.back();
        board->doMove(randomMove, our_side);
        possibleMoves.clear();
        return randomMove;
    }

    for(i = possibleMoves.begin(); i != possibleMoves.end(); i++)
    {
        Move * currentMove = *i;
        Board *boardCopy = board->copy();
        boardCopy->doMove(currentMove, our_side);
        int currentScore = boardCopy->getMoveScore(our_side);
        if(currentScore > maximumScore){
            maximumScore = currentScore;
            bestMoveLocation = i;
        }
    }

//    gettimeofday(&current_time, NULL); // define now
//    timersub(&current_time, &start_time, &result);
//    // calculate time_elapsed in milliseconds
//    time_elapsed = (result.tv_usec / 1000.0) + (result.tv_sec * 1000.0);

    if(not possibleMoves.empty()) // otherwise, bestMoveLocation is nonsensical
    {
        // now do the best move, located at bestMoveLocation
        Move * bestMove = *bestMoveLocation;
        board->doMove(bestMove, our_side);

        // clear out our moves vector
        possibleMoves.clear();
        return bestMove;
    }
    return NULL;
}
    
