#ifndef __EXAMPLEPLAYER_H__
#define __EXAMPLEPLAYER_H__

#include <iostream>
#include "common.h"
#include "board.h"
#include <vector>
#include <sys/time.h>

using namespace std;

class ExamplePlayer {

private:
    Board *board;
    Side our_side;
    Side their_side;

    vector<Move *> possibleMoves;

// we have created extra initializations here.

public:
    ExamplePlayer(Side side);
    ~ExamplePlayer();
    Move *doMove(Move *opponentsMove, int msLeft);
};

#endif
