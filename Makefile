CC          = g++
CFLAGS      = -Wall -ansi -pedantic -ggdb
OBJS        = exampleplayer.o wrapper.o board.o
PASSIONFRUIT  = player

all: $(PASSIONFRUIT) testgame
	
$(PASSIONFRUIT): $(OBJS)
	$(CC) -o $@ $^
        
TestGame: testgame.o
	$(CC) -o $@ $^
        
%.o: %.cpp
	$(CC) -c $(CFLAGS) -x c++ $< -o $@
	
java:
	make -C java/

cleanjava:
	make -C java/ clean

clean:
	rm -f *.o $(PASSIONFRUIT) testgame
	
.PHONY: java
