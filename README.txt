Team Passionfruit:
Tara Sowrirajan
Heather Gold

https://bitbucket.org/passionfruit/passionfruit-cs2_othello/overview

Work we did:

Please note: we both worked on most of this project together, "side by side", so it is hard to
pinpoint individual contributions since much of the work was done in conjunction.

What Tara did:
    Made the vector of moves into a vector of move pointers.
    Fixed the bug that caused our AI to make invalid moves and made the AI make valid moves.
    Initialized the board and specified our code to our team. Fixed merge conflicts several times.
    Implemented half of the helper function in the board class that evaluates the board using our scoring
    heuristic. Created new methodology to beat SimplePlayer. Created and tested time mechanism.


What Heather did:
    Fixed a memory leak by clearing the vector of possible moves. Did debugging to make
    progress towards fixing the AI and other types of invalid moves. Fixed merge conflicts several times.
    Implemented the other half of the helper function in the board class that evaluates the board using our scoring
    heuristic. Created new methodology to beat SimplePlayer. Created time mechanism that counts time in milliseconds.


What we did to improve our AI:
    We now keep track of time in milliseconds using gettimeofday() and timeval. We verify that we have enough time
    to run our scoring helper function by checking time left against time elased. If we don't have enough time, we
    simply return a random move from our vector of possible moves. We ran tests and printed out the time taken to
    run the scoring function in order to come up with the number used in our program. This construct allows us to
    always be able to make a move and we will not time out. We developed a heuristic in order to focus on making
    good moves. This is done by weighting the corners of the board, putting negative weights on the pieces adjacent
    to corners (by the corner), and certain other advantageous or disadvantageous weights on other coordinates of
    the board. Then, we pick the possible move with the best weighted score after going through all the weighted
    scores for each possible move. This guareentees that we can reliably beat SimplePlayer.

